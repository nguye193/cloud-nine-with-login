package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testPasswordLengthRegular() {
		
		boolean result = LoginValidator.isValidLoginPassword("abcde123");
		assertTrue(  "Invalid length" , result );
	}
	
	@Test(expected = AssertionError.class)
	public void testPasswordLengthException() {
		
		boolean result = LoginValidator.isValidLoginPassword("a");
		assertTrue(  "Invalid length" , result );
	}
	
	@Test
	public void testPasswordLengthBoundaryIn() {
		
		boolean result = LoginValidator.isValidLoginPassword("abc123");
		assertTrue(  "Invalid length" , result );
	}
	
	@Test(expected = AssertionError.class)
	public void testPasswordLengthBoundaryOut() {
		
		boolean result = LoginValidator.isValidLoginPassword("de123");
		assertTrue(  "Invalid length" , result );
	}
	
	
	@Test
	public void testPasswordCharactersRegular() {
		
		boolean result = LoginValidator.isValidLoginPassword("abcde123");
		assertTrue(  "Invalid length" , result );
	}
	
	@Test
	public void testPasswordCharactersException() {
		
		boolean result = LoginValidator.isValidLoginPassword("abcdefhg");
		assertTrue(  "Invalid length" , result );
	}
	
	@Test
	public void testPasswordCharactersBoundaryIn() {
		
		boolean result = LoginValidator.isValidLoginPassword("abcdf1");
		assertTrue(  "Invalid length" , result );
	}
	
	@Test
	public void testPasswordCharactersBoundaryOut() {
		
		boolean result = LoginValidator.isValidLoginPassword("123abc");
		assertTrue(  "Invalid length" , result );
	}	
}


